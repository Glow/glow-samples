#include "MaterialSample.hh"

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/util/DefaultShaderParser.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/material/IBL.hh>
#include <glow-extras/pipeline/RenderPipeline.hh>
#include <glow-extras/pipeline/RenderScene.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include <glm/ext.hpp>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

namespace
{
bool recreateIBL = false;
bool applyCallback = false;

void TW_CALL setRecreateIBL(void* p)
{
    // Recreate IBL next frame
    recreateIBL = true;
}
}

void MaterialSample::ApplyCallback()
{
    onResize(getCamera()->getViewportWidth(), getCamera()->getViewportHeight());
}

void MaterialSample::RecreateIBL()
{
    mEnvLUT = material::IBL::createEnvLutGGX();

    for (auto& t : mEnvMap)
        t = nullptr;

    {
        auto s = mShaderOpaque->use();
        material::IBL::initShaderGGX(s, mEnvLUT);
    }
    {
        auto s = mShaderTransparent->use();
        material::IBL::initShaderGGX(s, mEnvLUT);
    }
}

void MaterialSample::init()
{
    glow::warning() << " ---- WARNING ---- ";
    glow::warning() << "This sample was ported from the old rendering pipeline and is obsolete";
    glow::warning() << "For an up to date way of using GGX+IBL materials with the Pipeline, take a look at "
                       "wip/RenderingPipelineSample";
    glow::warning() << " ---- WARNING ---- ";

    setGui(GlfwApp::Gui::AntTweakBar);
    setUsePipeline(true);

    GlfwApp::init(); // call to base!

    // Required for Material to actually work
    DefaultShaderParser::addIncludePath(util::pathOf(__FILE__) + "/../../../glow-extras/material/shader");

    mNamesObj.resize(OBJECT_COUNT);
    mObjects.resize(OBJECT_COUNT);

    mNamesTex.resize(TEXTURE_COUNT);
    mTexColor.resize(TEXTURE_COUNT);
    mTexNormal.resize(TEXTURE_COUNT);

    mNamesEnv.resize(ENVIRONMENT_COUNT);
    mTexEnv.resize(ENVIRONMENT_COUNT);
    mEnvMap.resize(ENVIRONMENT_COUNT);

    mObjects[OBJ_CUBE] = geometry::Cube<>().generate();
    mNamesObj[OBJ_BUNNY] = "bunny-smooth.obj";
    mNamesObj[OBJ_SPHERE] = "sphere-detailed.obj";
    mNamesObj[OBJ_SUZANNE] = "suzanne.obj";
    mNamesObj[OBJ_TORUS] = "torus.obj";
    mNamesObj[OBJ_PLANE] = "plane.obj";
    mNamesObj[OBJ_CONE] = "cone.obj";
    mNamesObj[OBJ_CYLINDER] = "cylinder.obj";
    mNamesObj[OBJ_ICOSPHERE] = "icosphere.obj";
    mNamesObj[OBJ_META] = "meta.obj";

    mNamesTex[TEX_WHITE] = "white";
    mNamesTex[TEX_CRYSTA] = "crysta";
    mNamesTex[TEX_BLOCKY] = "blocky";
    mNamesTex[TEX_DIAMOND_PLATE] = "diamondplate";
    mNamesTex[TEX_METALSTORM] = "metalstorm";
    mNamesTex[TEX_PANELS] = "panels";
    mNamesTex[TEX_TILES] = "tiles";

    mNamesEnv[ENV_GOLDEN_GATE] = "golden-gate";
    mNamesEnv[ENV_CHAPEL] = "chapel";
    mNamesEnv[ENV_ICE_RIVER] = "ice-river";
    mNamesEnv[ENV_MOUNTAIN] = "mountain";
    mNamesEnv[ENV_PARK] = "park";
    mNamesEnv[ENV_POND] = "pond";
    mNamesEnv[ENV_STAIRS] = "stairs";

    mLightSphere = assimp::Importer().load(util::pathOf(__FILE__) + "/../../../data/sphere-detailed.obj");

    TwEnumVal objectEV[] = {
        {OBJ_BUNNY, "Bunny"},         //
        {OBJ_CUBE, "Cube"},           //
        {OBJ_SPHERE, "Sphere"},       //
        {OBJ_SUZANNE, "Suzanne"},     //
        {OBJ_TORUS, "Torus"},         //
        {OBJ_PLANE, "Plane"},         //
        {OBJ_CONE, "Cone"},           //
        {OBJ_CYLINDER, "Cylinder"},   //
        {OBJ_ICOSPHERE, "Icosphere"}, //
        {OBJ_META, "Meta"},           //
    };
    TwEnumVal textureEV[] = {
        {TEX_WHITE, "White"},                 //
        {TEX_CRYSTA, "Crysta"},               //
        {TEX_BLOCKY, "Blocky"},               //
        {TEX_DIAMOND_PLATE, "Diamond Plate"}, //
        {TEX_METALSTORM, "Metalstorm"},       //
        {TEX_PANELS, "Panels"},               //
        {TEX_TILES, "Tiles"},                 //
    };
    TwEnumVal envEV[] = {
        {ENV_GOLDEN_GATE, "Golden Gate"}, //
        {ENV_CHAPEL, "Chapel"},           //
        {ENV_ICE_RIVER, "Ice River"},     //
        {ENV_MOUNTAIN, "Mountain"},       //
        {ENV_PARK, "Park"},               //
        {ENV_POND, "Pond"},               //
        {ENV_STAIRS, "Green Stairs"},     //
    };

    TwAddVarRW(tweakbar(), "Environment", TwDefineEnum("EnvType", envEV, ENVIRONMENT_COUNT), &mCurrEnv, "group=material");
    TwAddVarRW(tweakbar(), "Object", TwDefineEnum("ObjectType", objectEV, OBJECT_COUNT), &mCurrObject, "group=material");
    TwAddVarRW(tweakbar(), "Object Scale", TW_TYPE_FLOAT, &mObjectScale, "group=material min=0.1 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Texture", TwDefineEnum("TextureType", textureEV, TEXTURE_COUNT), &mCurrTexture, "group=material");
    TwAddVarRW(tweakbar(), "Texture Scale", TW_TYPE_FLOAT, &mTextureScale, "group=material min=0.1 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Color Filter", TW_TYPE_COLOR3F, &mColor, "group=material");
    TwAddVarRW(tweakbar(), "Metallic", TW_TYPE_BOOLCPP, &mIsMetallic, "group=material");
    TwAddVarRW(tweakbar(), "Roughness", TW_TYPE_FLOAT, &mRoughness, "group=material min=0 max=1 step=0.01");
    TwAddVarRW(tweakbar(), "Alpha", TW_TYPE_FLOAT, &mAlpha, "group=material min=0 max=1 step=0.01");
    TwAddButton(tweakbar(), "Recreate IBL", setRecreateIBL, nullptr, " group=material");

    TwAddVarRW(tweakbar(), "Light Distance", TW_TYPE_FLOAT, &mLightDistance, "group=light min=0.1 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Light Direction", TW_TYPE_DIR3F, &mLightDir, "group=light");
    TwAddVarRW(tweakbar(), "Light Size", TW_TYPE_FLOAT, &mLightSize, "group=light min=0.1 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Light Radius", TW_TYPE_FLOAT, &mLightRadius, "group=light min=0.1 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Light Color", TW_TYPE_COLOR3F, &mLightColor, "group=light");

    mShaderOpaque = Program::createFromFiles({util::pathOf(__FILE__) + "/shader-opaque.fsh", //
                                              util::pathOf(__FILE__) + "/shader.vsh"});
    mShaderLight = Program::createFromFiles({util::pathOf(__FILE__) + "/shader-light.fsh", //
                                             util::pathOf(__FILE__) + "/shader.vsh"});
    mShaderTransparent = Program::createFromFiles({util::pathOf(__FILE__) + "/shader-transparent.fsh", //
                                                   util::pathOf(__FILE__) + "/shader.vsh"});
    mShaderBG = Program::createFromFile(util::pathOf(__FILE__) + "/bg");

    mQuad = geometry::Quad<>().generate();


    //    mFXAA = getPipeline()->getFXAA();
    //    mDitheringStrength = getPipeline()->getDitheringStrength();
    //    mTransparency = getPipeline()->getTransparentPass();

    // material init
    RecreateIBL();
}

void MaterialSample::onResize(int w, int h)
{
    getCamera()->setViewportSize(w, h);
}

void MaterialSample::setupObjectShader(glow::UsedProgram& shader, glow::pipeline::CameraData const& d)
{
    mMetallic = mIsMetallic ? 1.0f : 0.0f;

    // geometry
    shader.setUniform("uRuntime", mRuntime);
    shader.setUniform("uView", d.view);
    shader.setUniform("uProj", d.proj);
    shader.setUniform("uCamPos", d.camPos);
    auto model = glm::scale(glm::vec3(mObjectScale));
    if (mCurrObject == OBJ_BUNNY)
        model = model * glm::translate(glm::vec3(0, -.6, 0));
    shader.setUniform("uModel", model);

    // material
    shader.setUniform("uColor", mColor);
    shader.setUniform("uRoughness", mRoughness);
    shader.setUniform("uMetallic", mMetallic);
    shader.setUniform("uAlpha", mAlpha);
    shader.setUniform("uTextureScale", mTextureScale);
    shader.setTexture("uTextureColor", mTexColor[mCurrTexture]);
    shader.setTexture("uTextureNormal", mTexNormal[mCurrTexture]);

    // light
    shader.setUniform("uLightDir", mLightDir);
    shader.setUniform("uLightDistance", mLightDistance);
    shader.setUniform("uLightColor", mLightColor);
    shader.setUniform("uLightSize", mLightSize);
    shader.setUniform("uLightRadius", mLightRadius);

    // environment map
    material::IBL::prepareShaderGGX(shader, mEnvMap[mCurrEnv]);
}

void MaterialSample::onRenderOpaquePass(glow::pipeline::RenderContext const& ctx)
{
    if (true)
    { // bg
        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);
        auto shader = ctx.useProgram(mShaderBG);

        shader.setTexture("uTexture", mTexEnv[mCurrEnv]);
        shader.setUniform("uInvProj", ctx.camData.cleanProjInverse);
        shader.setUniform("uInvView", ctx.camData.viewInverse);

        mQuad->bind().draw();
    }
    if (true)
    { // light
        auto shader = ctx.useProgram(mShaderLight);

        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);
        shader.setUniform("uColor", mLightColor);
        shader.setUniform("uModel", glm::translate(mLightDir * mLightDistance) * glm::scale(glm::vec3(mLightSize)));

        mLightSphere->bind().draw();
    }
    if (mAlpha > 0.999f)
    { // scene
        auto shader = ctx.useProgram(mShaderOpaque);
        setupObjectShader(shader, ctx.camData);

        mObjects[mCurrObject]->bind().draw();
    }
}

void MaterialSample::onRenderTransparentPass(glow::pipeline::RenderContext const& ctx)
{
    if (mAlpha <= 0.999f)
    { // scene
        auto shader = ctx.useProgram(mShaderTransparent);
        setupObjectShader(shader, ctx.camData);

        mObjects[mCurrObject]->bind().draw();
    }
}

void MaterialSample::render(float elapsedSeconds)
{
    mRuntime += elapsedSeconds;
    if (recreateIBL)
    {
        RecreateIBL();
        recreateIBL = false;
    }
    if (applyCallback)
    {
        ApplyCallback();
        applyCallback = false;
    }

    // on demand loading
    if (!mTexColor[mCurrTexture])
        mTexColor[mCurrTexture]
            = Texture2D::createFromFile(util::pathOf(__FILE__) + "/../../../data/textures/" + mNamesTex[mCurrTexture] + ".color.png", ColorSpace::sRGB);
    if (!mTexNormal[mCurrTexture])
        mTexNormal[mCurrTexture]
            = Texture2D::createFromFile(util::pathOf(__FILE__) + "/../../../data/textures/" + mNamesTex[mCurrTexture] + ".normal.png", ColorSpace::Linear);
    if (!mObjects[mCurrObject])
        mObjects[mCurrObject] = assimp::Importer().load(util::pathOf(__FILE__) + "/../../../data/" + mNamesObj[mCurrObject]);
    if (!mTexEnv[mCurrEnv])
    {
        auto pbt = util::pathOf(__FILE__) + "/../../../data/cubemap/" + mNamesEnv[mCurrEnv];
        mTexEnv[mCurrEnv] = TextureCubeMap::createFromData(TextureData::createFromFileCube( //
            pbt + "/posx.jpg",                                                              //
            pbt + "/negx.jpg",                                                              //
            pbt + "/posy.jpg",                                                              //
            pbt + "/negy.jpg",                                                              //
            pbt + "/posz.jpg",                                                              //
            pbt + "/negz.jpg",                                                              //
            ColorSpace::sRGB));
    }
    if (!mEnvMap[mCurrEnv])
        mEnvMap[mCurrEnv] = material::IBL::createEnvMapGGX(mTexEnv[mCurrEnv]);

    //    getPipeline()->setFXAA(mFXAA);
    //    getPipeline()->setDitheringStrength(mDitheringStrength);
    //    getPipeline()->setTransparentPass(mTransparency);

    GlfwApp::render(elapsedSeconds);
}
