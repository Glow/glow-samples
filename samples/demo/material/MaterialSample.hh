#pragma once

#include <glow/fwd.hh>

#include <glm/glm.hpp>

#include <glow-extras/glfw/GlfwApp.hh>

#include <AntTweakBar.h>

#include <glow/objects/Program.hh>

/**
 * WARNING! This Sample was ported over from the now deprecated old Pipeline
 * Some features might not work as expected
 *
 * For a more up to date look at how to realize IBL+GGX materials, take a look at wip/RenderingPipelineSample
 */
class MaterialSample : public glow::glfw::GlfwApp
{
public:
    MaterialSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    std::vector<std::string> mNamesTex;
    std::vector<std::string> mNamesObj;
    std::vector<std::string> mNamesEnv;


    glow::SharedProgram mShaderOpaque;
    glow::SharedProgram mShaderTransparent;
    glow::SharedProgram mShaderLight;
    std::vector<glow::SharedVertexArray> mObjects;
    std::vector<glow::SharedTexture2D> mTexColor;
    std::vector<glow::SharedTexture2D> mTexNormal;

    glow::SharedProgram mShaderBG;
    glow::SharedVertexArray mQuad;
    glow::SharedVertexArray mLightSphere;
    std::vector<glow::SharedTextureCubeMap> mTexEnv;

    glow::SharedTexture2D mEnvLUT;
    std::vector<glow::SharedTextureCubeMap> mEnvMap;

    glm::vec3 mColor = glm::vec3(1, 1, 1);
    float mMetallic = 0.f;
    float mRoughness = 0.2f;
    float mObjectScale = 1.0f;
    float mTextureScale = 1.0f;

    bool mIsMetallic = true;

    float mAlpha = 1.f;

    glm::vec3 mLightDir = glm::normalize(glm::vec3(1, 5, .5));
    float mLightDistance = 5.0f;
    float mLightSize = 1.0f;
    glm::vec3 mLightColor = glm::vec3(1, 1, 1);
    float mLightRadius = 9.0f;

    float mRuntime = 0.0f;
    // bool mAnimate = true;


    enum ObjectType
    {
        OBJ_BUNNY,
        OBJ_CUBE,
        OBJ_SPHERE,
        OBJ_SUZANNE,
        OBJ_TORUS,
        OBJ_PLANE,
        OBJ_CONE,
        OBJ_CYLINDER,
        OBJ_ICOSPHERE,
        OBJ_META,

        OBJECT_COUNT
    };

    enum TextureType
    {
        TEX_WHITE,

        TEX_CRYSTA,
        TEX_BLOCKY,
        TEX_DIAMOND_PLATE,
        TEX_METALSTORM,
        TEX_PANELS,
        TEX_TILES,

        TEXTURE_COUNT
    };

    enum EnvType
    {
        ENV_GOLDEN_GATE,
        ENV_CHAPEL,
        ENV_ICE_RIVER,
        ENV_MOUNTAIN,
        ENV_PARK,
        ENV_POND,
        ENV_STAIRS,

        ENVIRONMENT_COUNT
    };

    ObjectType mCurrObject = ObjectType::OBJ_CUBE;
    TextureType mCurrTexture = TextureType::TEX_WHITE;
    EnvType mCurrEnv = EnvType::ENV_GOLDEN_GATE;

    void ApplyCallback();
    void RecreateIBL();
    void setupObjectShader(glow::UsedProgram& shader, glow::pipeline::CameraData const& d);

protected:
    void init() override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;

    void onRenderOpaquePass(glow::pipeline::RenderContext const& ctx) override;
    void onRenderTransparentPass(glow::pipeline::RenderContext const& ctx) override;
};
