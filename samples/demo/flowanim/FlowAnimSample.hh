#pragma once

#include <vector>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class FlowAnimSample : public glow::glfw::GlfwApp
{
public:
    FlowAnimSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    float mTime = 0.0f;
    float mAnim = 0.07f;
    int mSteps = 5;

    glow::SharedTexture3D mNoise;

    glow::SharedTextureRectangle mResultA;
    glow::SharedTextureRectangle mResultB;
    glow::SharedFramebuffer mFBOA;
    glow::SharedFramebuffer mFBOB;

    glow::SharedProgram mShader;
    glow::SharedProgram mShaderOutput;
    glow::SharedVertexArray mQuad;

public:
    void recompute();

protected:
    void init() override;
    void update(float elapsedSeconds) override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;
};
