#include <uniforms.glsl>

in vec3 aPosition;
in float aLength;

out vec3 gPosition;
out float gLength;

void main() {
    gLength = aLength;
    gPosition = aPosition;
    gl_Position = uModel * vec4(aPosition, 1.0);
}
