#include <shader_globals.hh>
layout(local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE, local_size_z = 1) in;

uniform layout(binding=0, rgba8) restrict writeonly image2D uTexture;

void main()
{
    imageStore(uTexture, ivec3(gl_GlobalInvocationID).xy, ivec4(1));
}
