#include <shader_globals.hh>
layout(local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE, local_size_z = 1) in;

uniform layout(binding=0, rgba8) restrict image2D uTexture;

#include <grass_globals.glsl>

struct Trampler {
    vec2 pos;
    vec2 velocity;
};

layout(std430) buffer sTrampleInfo
{
    vec2 mowerPosition;
    float mowerRange2;

    //Trampler tramplers[];
} ssboTrampleInfo;

void main()
{
    const ivec2 texel = ivec3(gl_GlobalInvocationID).xy;
    const vec2 worldPos = getWorldPosition(texel);
    vec4 outputValue = imageLoad(uTexture, texel);

    vec2 mowerDelta = ssboTrampleInfo.mowerPosition - worldPos;
    float inMowerRange = step(length2(mowerDelta), ssboTrampleInfo.mowerRange2);
    float grassMowed = min(outputValue.w, 1.0 - inMowerRange);

    imageStore(uTexture, texel, ivec4(1, 1, 1, grassMowed));
}
