#include "VoxelSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/debugging/DebugRenderer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include <AntTweakBar.h>

#include <GLFW/glfw3.h>

using namespace glow;

void VoxelSample::refresh()
{
    mVoxels.updateLighting();
    mVoxelVAO = mVoxels.buildMesh(mVoxelBuffer);
}

void VoxelSample::init()
{
    setUsePipeline(true);

    setGui(GlfwApp::Gui::AntTweakBar);

    // IMPORTANT: call base function
    glfw::GlfwApp::init();

    // mDebugLight = std::make_shared<debugging::DebugRenderer>();

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mVoxelBuffer = ShaderStorageBuffer::create();

    mVoxels.generate();

    mShader->setShaderStorageBuffer("bVoxels", mVoxelBuffer);

    TwAddVarRW(tweakbar(), "Light Direction", TW_TYPE_DIR3F, &mLightDir, "group=light");
    TwAddVarRW(tweakbar(), "Use Occlusion", TW_TYPE_BOOLCPP, &mUseOcclusion, "group=rendering");
    TwAddVarRW(tweakbar(), "Use Lighting", TW_TYPE_BOOLCPP, &mUseLighting, "group=rendering");
    TwAddVarRW(tweakbar(), "Render Voxels", TW_TYPE_BOOLCPP, &mRenderVoxels, "group=rendering");
    TwAddVarRW(tweakbar(), "Lightmap Dir", TW_TYPE_INT32, &mDebugLightDir, "group=debug");
    TwAddVarRW(tweakbar(), "Lightmap Lvl", TW_TYPE_INT32, &mDebugLightLevel, "group=debug");

    // cam init
    auto cam = getCamera();
    cam->setLookAt({10, 10, 10}, {0, 0, 0});

    // initial refresh
    refresh();
}


void VoxelSample::update(float elapsedSeconds)
{
    // IMPORTANT: call base function
    glfw::GlfwApp::update(elapsedSeconds);
}

void VoxelSample::onRenderOpaquePass(pipeline::RenderContext const& ctx)
{
    // debug light
    //    if (mDebugLightDir >= 0 && mDebugLightLevel >= 0)
    //    {
    //        static int lastDir = -1;
    //        static int lastLvl = -1;
    //        if (lastDir != mDebugLightDir || lastLvl != mDebugLightLevel)
    //        {
    //            lastDir = mDebugLightDir;
    //            lastLvl = mDebugLightLevel;
    //            mDebugLight->clear();

    //            auto size = Voxels::Size >> mDebugLightLevel;
    //            auto cs = 1 << mDebugLightLevel;
    //            auto const& map = mVoxels.lightmap[mDebugLightDir][mDebugLightLevel];
    //            glm::vec3 vs(cs, cs, cs);
    //            if (mDebugLightDir / 2 == 0)
    //                vs.x = 0.4f;
    //            if (mDebugLightDir / 2 == 1)
    //                vs.y = 0.4f;
    //            if (mDebugLightDir / 2 == 2)
    //                vs.z = 0.4f;
    //            for (auto z = 0; z < size; ++z)
    //                for (auto y = 0; y < size; ++y)
    //                    for (auto x = 0; x < size; ++x)
    //                    {
    //                        auto const& d = map[mVoxels.lightIdxOf(x, y, z, mDebugLightLevel)];
    //                        if (d.opacity > 0 && length(d.emission) > 0)
    //                        {
    //                            glm::vec4 color(d.emission, 0.2f + d.opacity * 0.3f);
    //                            mDebugLight->renderAABBCentered(glm::vec3(x, y, z) * cs + cs / 2.0f - Voxels::Size / 2.0f, vs, color);
    //                        }
    //                    }
    //        }

    //        mDebugLight->render(pass);
    //    }

    if (mRenderVoxels)
    {
        auto shader = ctx.useProgram(mShader);

        shader.setUniform("uUseOcclusion", mUseOcclusion);
        shader.setUniform("uUseLighting", !mUseLighting);
        shader.setUniform("uLightDir", mLightDir);
        shader.setUniform("uRuntime", getCurrentTime());
        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);
        shader.setUniform("uModel", glm::translate(glm::vec3(-Voxels::Size / 2)));

        mVoxelVAO->bind().draw();
    }
}


bool VoxelSample::onMouseButton(double x, double y, int button, int action, int mods, int clickCount)
{
    // IMPORTANT: call base function
    if (GlfwApp::onMouseButton(x, y, button, action, mods, clickCount))
        return true;

    // Ctrl+[LMB] = select btn
    if (button == GLFW_MOUSE_BUTTON_LEFT && mods == GLFW_MOD_CONTROL && action == GLFW_PRESS)
    {
        // glm::vec3 pos;
        // float depth;

        //        if (getPipeline()->queryPosition3D(x, y, &pos, &depth))
        //        {
        //            auto d = normalize(pos - getCamera()->getPosition());
        //            mSelectedPos = pos + Voxels::Size / 2.0f + d * 0.01f;
        //            mSelectedCube = glm::ivec3(glm::floor(mSelectedPos));

        // glow::info() << "Selected " << to_string(mSelectedCube) << " (clicked " << to_string(mSelectedPos) << ")";

        //            refresh();
        //            return true;
        //        }
    }

    return false;
}
