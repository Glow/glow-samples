#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

struct GpuVoxel
{
	vec3 normal;
	mat4 colors;
	mat4 lights;
};

layout(std430, binding = 0) buffer bVoxels
{
    GpuVoxel voxels[];
};

uniform vec3 uLightDir;
uniform bool uUseLighting;
uniform bool uUseOcclusion;

in vec3 vPosition;
in vec2 vUV;
flat in int vIdx;

void main() 
{
    vec3 outColor;

    GpuVoxel v = voxels[vIdx];
    
    float x = vUV.x;
    float y = vUV.y;
    vec4 quad = vec4( (1-x)*(1-y), (1-x)*y, x*(1-y), x*y );

    vec4 color = v.colors * quad;
    vec4 light = v.lights * quad;
    float occlusion = light.w;

    if (uUseOcclusion)
        color *= occlusion;

    if (uUseLighting)
        outColor = color.rgb * light.rgb;
    else
    {
        vec3 L = normalize(uLightDir);
        outColor = color.rgb * (0.5 + 0.5 * dot(L, normalize(v.normal)));
    }

    outputOpaqueGeometry(outColor);
}
