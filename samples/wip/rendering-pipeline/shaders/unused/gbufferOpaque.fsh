#include <glow-pipeline/pass/opaque/lighting.glsl>

uniform vec3 uCamPos;

uniform float uMetallic;
uniform float uRoughness;
uniform vec3 uAlbedo;

in vec3 vPosition;
in vec3 vNormal;

out vec3 fHDR;

vec3 shadingDiffuseOrenNayar(vec3 N, vec3 V, vec3 L, float roughness, vec3 albedo) 
{
    float dotVL = dot(L, V);
    float dotLN = dot(L, N);
    float dotNV = dot(N, V);

    float s = dotVL - dotLN * dotNV;
    float t = mix(1.0, max(dotLN, dotNV), step(0.0, s));

    float sigma2 = roughness * roughness;
    vec3 A = 1.0 + sigma2 * (albedo / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));    
    float B = 0.45 * sigma2 / (sigma2 + 0.09);

    return albedo * max(0.0, dotLN) * (A + B * s / t);
}

void main() {
    vec3 N = normalize(vNormal);
    vec2 velocity = vec2(0);
    float ao = 0.f;
    vec3 V = normalize(uCamPos - vPosition);
    vec3 L = normalize(vec3(1, 1, 0));

    fHDR = vec3(1, 1, 1) * shadingDiffuseOrenNayar(N, V, L, uRoughness, uAlbedo);
    fHDR.r = getLightCountOfCluster() / 4.f;
    fHDR = getClusterDebugColor();

    //outputMaterial(uAlbedo, normal, velocity, uMetallic, uRoughness, ao);

    // TEST Checker Pattern
    // fColor *= float((uint(gl_FragCoord.x) + uint(gl_FragCoord.y)) % 2 == 0);
}
