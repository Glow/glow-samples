layout(local_size_x = 2, local_size_y = 2, local_size_z = 2) in;

uniform layout(r32i, binding=0) iimage3D uVolumeR;
uniform layout(r32i, binding=1) iimage3D uVolumeG;
uniform layout(r32i, binding=2) iimage3D uVolumeB;
uniform layout(r32i, binding=3) iimage3D uVolumeA;
uniform layout(rgba16f, binding=4) writeonly image3D uOut;

void main()
{
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint z = gl_GlobalInvocationID.z;

    ivec3 s = imageSize(uOut);

    if (x >= s.x || y >= s.y || z >= s.z)
        return; // out of bounds

    vec4 data;
    data.r = float(imageLoad(uVolumeR, ivec3(x, y, z))) / (1000.f * 1000.f);
    data.g = float(imageLoad(uVolumeG, ivec3(x, y, z))) / (1000.f * 1000.f);
    data.b = float(imageLoad(uVolumeB, ivec3(x, y, z))) / (1000.f * 1000.f);
    data.a = float(imageLoad(uVolumeA, ivec3(x, y, z))) / 1000.f;

    // normalize
    if (data.a > 1)
    {
        data.rgb /= data.a;
        data.a = 1;
    }

    imageStore(uOut, ivec3(x, y, z), data);
}
