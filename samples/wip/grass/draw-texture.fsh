uniform sampler2D uTexture;

in vec2 vPosition;

out vec4 fColor;

void main() {
    fColor = texture(uTexture, vPosition).rgba;
}
