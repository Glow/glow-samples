#glow pipeline transparent

#include <glow-pipeline/pass/transparent/oitPass.glsl>

uniform vec3 uColor;
uniform float uAlpha;
uniform int uVolumeSize;

in vec4 vColor;
in vec3 vPosition;
in vec3 vDirection;

void main() {
    vec4 color = vColor;
    color.rgb *= vPosition.y / float(uVolumeSize);
    color.rgb *= pow(max(0, dot(vDirection, vec3(0,1,0))), 2);
    outputOitGeometry(gl_FragCoord.z, color.rgb, color.a);
}
