#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

uniform sampler3D uTexVolume;
uniform sampler3D uTexVolumeDirs;

uniform vec3 uLightDir;
uniform vec3 uCamPos;
uniform vec3 uSpecularColor;
uniform float uSpecularExponent;

uniform int uVolumeResolution;
uniform float uCellWidth;

uniform vec3 uVolumeMin;
uniform vec3 uVolumeMax;
uniform float uTextureScale = 1.0;

in vec3 vWorldSpacePos;

const int MAX_STEPS = 64 * 3;

vec3 getBladeLighting(in vec3 L, in vec3 direction, in vec3 position)
{
    vec3 T = normalize(direction + vec3(0,0.001,0));
    vec3 V = normalize(position - uCamPos);
    vec3 H = normalize(L + V);

    float dotTH = dot(T, H);
    float sinTH = sqrt(1.0 - dotTH * dotTH);
    float dirAtten = smoothstep(-1.0, 0.0, dot(T, H));
    float spec = dirAtten * pow(sinTH, uSpecularExponent);

    return spec * uSpecularColor;
}

void main() {

    vec3 dir = normalize(vWorldSpacePos - uCamPos);

    vec3 pos = vWorldSpacePos;
    float stepSize = 1.0 / float(uVolumeResolution);
    vec3 dstep = normalize(dir) * stepSize;

    // A = 1 - (1 - A_0)^(s_0 / s)
    // C^_i = (1 - A^_{i-1}) C_i + C^_{i-1}
    // A^_i = (1 - A^_{i-1}) A_i + A^_{i-1}

    vec3 outputColor = vec3(0);
    float alpha = 0;

    vec3 L = normalize(uLightDir);
    const float shadowMultiplier = 1 / (uVolumeMax.y - uVolumeMin.y);

    for (int i = 0; i < MAX_STEPS; ++i)
    {
        float normalizedY = (pos.y - uVolumeMin.y) * shadowMultiplier;
        const vec3 samplePos = vec3(pos.x * uTextureScale, normalizedY, pos.z * uTextureScale);
        vec4 color = texture(uTexVolume, samplePos);
        vec4 dir = texture(uTexVolumeDirs, samplePos);

        // Lighting
        color.rgb += getBladeLighting(L, dir.xyz, pos) * color.a;

        // Shadowing
        color.rgb *= normalizedY;

        // Debug output
        //color.rgb = abs(normalize(dir.xyz + vec3(0,0.001,0))) * color.a;

        // Accumulation
        outputColor += (1 - alpha) * color.rgb;
        alpha += (1 - alpha) * color.a;

        // Ray step
        pos += dstep;

        // If the ray has either left the volume or the alpha is 1, break
        if (any(greaterThan(pos, uVolumeMax)) ||
            any(lessThan(pos, uVolumeMin)) ||
            alpha > 0.99)
            break;
    }

    // Discard low-alpha fragments (This is an opaque pass)
    if (alpha < 0.4)
        discard;

    // Output color to pipeline
    outputOpaqueGeometry(outputColor);
}
