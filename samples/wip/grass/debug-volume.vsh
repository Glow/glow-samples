uniform int uVolumeSize;

uniform layout(rgba16f, binding=0) readonly image3D uVolume;
uniform layout(rgba16f, binding=1) readonly image3D uVolumeDirs;

in vec3 aPosition;

out vec4 vColor;
out vec3 vPosition;
out vec3 vDirection;

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uModel;

void main() {
    int id = int(gl_InstanceID);
    int x = id % uVolumeSize;
    int y = (id / uVolumeSize) % uVolumeSize;
    int z = id / (uVolumeSize * uVolumeSize);

    vec3 pos = aPosition * .5 + .5 + vec3(x, y, z);
    pos /= float(uVolumeSize);
    pos -= vec3(.5, 0, .5);

    vColor = imageLoad(uVolume, ivec3(x,y,z));
    vDirection = imageLoad(uVolumeDirs, ivec3(x,y,z)).xyz;

    vPosition = vec3(x,y,z);

    if (vColor.a <= 0.001) // acceleration
        pos = vec3(0);

    gl_Position = uProj * uView * uModel * vec4(pos, 1.0);
}
