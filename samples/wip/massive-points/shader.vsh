in float x;
in float y;
in float z;

// out vec3 vColor;

uniform mat4 uViewProj;

void main() 
{
    // vec3 pos = vec3(
    //     aPosition % 512,
    //     aPosition / 512 % 512,
    //     aPosition / 512 / 512
    // ) / 511.0;
    // vColor = pos;    
    vec3 scaledPos = 10 * vec3(x,y,z);
    gl_Position = uViewProj * vec4(scaledPos, 1.0);
}
