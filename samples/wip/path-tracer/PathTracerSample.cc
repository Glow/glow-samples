#include "PathTracerSample.hh"

#include <glow/gl.hh>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/util/DefaultShaderParser.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>

#include <GLFW/glfw3.h>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;


void PathTracerSample::resetTracer()
{
    mTotalSamples = 0;

    // clear accumulation
    auto fb = mFramebuffer->bind();
    GLOW_SCOPED(clearColor, 0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    //glow::debug() << "resetting";
}

void PathTracerSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init();


    // Required for Material to actually work
    DefaultShaderParser::addIncludePath(util::pathOf(__FILE__) + "/../../glow-extras/material/shader");

    mShaderTracer = Program::createFromFile(util::pathOf(__FILE__) + "/tracer");
    mShaderOutput = Program::createFromFile(util::pathOf(__FILE__) + "/output");
    mQuad = geometry::Quad<>().generate();

    mBuffer = TextureRectangle::create(2, 2, GL_RGB32F);
    mFramebuffer = Framebuffer::create({{"fColor", mBuffer}});

    TwAddVarRW(tweakbar(), "Max Bounces", TW_TYPE_INT32, &mMaxBounces, "");
    TwAddVarRW(tweakbar(), "Samples per Frame", TW_TYPE_INT32, &mSamplesPerFrame, "");
    TwAddVarRO(tweakbar(), "Total Samples", TW_TYPE_INT32, &mTotalSamples, "");

    resetTracer();
}

void PathTracerSample::render(float elapsedSeconds)
{
    // check if something changed
    auto viewProj = getCamera()->getProjectionMatrix() * getCamera()->getViewMatrix();
    if (viewProj != mLastViewProj)
    {
        mLastViewProj = viewProj;
        resetTracer();
    }
    if (lastBounces != mMaxBounces)
    {
        lastBounces = mMaxBounces;
        resetTracer();
    }

    // one sample
    {
        auto fb = mFramebuffer->bind();
        GLOW_SCOPED(disable, GL_DEPTH_TEST);    // no depth test
        GLOW_SCOPED(enable, GL_BLEND);          // blending on
        GLOW_SCOPED(blendFunc, GL_ONE, GL_ONE); // additive

        auto shader = mShaderTracer->use();
        auto invProj = inverse(getCamera()->getProjectionMatrix());
        auto invView = inverse(getCamera()->getViewMatrix());
        shader.setUniform("uInvProj", invProj);
        shader.setUniform("uInvView", invView);
        shader.setUniform("uMaxBounces", mMaxBounces);
        shader.setUniform("uCamPos", getCamera()->getPosition());
        shader.setUniform("uSeed", (uint32_t)rand());
        shader.setUniform("uViewport", glm::vec2(mWidth, mHeight));

        mQuad->bind().draw();
        ++mTotalSamples;
    }

    // output
    {
        auto shader = mShaderOutput->use();
        shader.setTexture("uTexture", mBuffer);
        shader.setUniform("uTotalSamples", mTotalSamples);

        mQuad->bind().draw();
    }
}

void PathTracerSample::onResize(int w, int h)
{
    getCamera()->setViewportSize(w, h);
    mBuffer->bind().resize(w, h);

    glViewport(0, 0, w, h);
    mWidth = w;
    mHeight = h;
}
