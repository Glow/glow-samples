in vec2 vPosition;

uniform sampler2DRect uTexture;
uniform int uTotalSamples;

out vec3 fColor;

void main() {
  vec3 color = texture(uTexture, gl_FragCoord.xy).rgb;
  fColor = pow(color / uTotalSamples, vec3(1 / 2.2));
}
