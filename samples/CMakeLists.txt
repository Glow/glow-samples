
if(MSVC)
    set(GLOW_SAMPLES_DEF_OPTIONS /MP /FC)
else()
    set(GLOW_SAMPLES_DEF_OPTIONS -Wall)
endif()

file(GLOB_RECURSE mains "main.cc")
foreach(main ${mains})
    get_filename_component(path ${main} DIRECTORY)
    string(REPLACE "${CMAKE_CURRENT_SOURCE_DIR}/" "" spath ${path})
    message(STATUS "[glow samples] adding sample ${spath}")
    add_subdirectory(${path})
endforeach()
