#version 430 core

#include "shader-header.glsl"

uniform float uCrustSize;

layout(points) in;
layout(points, max_vertices = 6) out;

in uint vPosition[1];

out uint gPosition;

ivec3 is = imageSize(uTexNearestPoint);

void checkNeighbor(ivec3 ip, int dx, int dy, int dz, vec3 refp, uint ptIdx)
{
    ivec3 np = ip + ivec3(dx,dy,dz);
    float dis = distance(refp, vec3(np));
    uint udis = dis2udis(dis);

    if (updateDistance(np, udis, ptIdx))
    {
        gPosition = (np.z * is.y + np.y) * is.x + np.x;
        EmitVertex();
    }
}

void main() 
{
    uint idx = vPosition[0];
    ivec3 ip = ivec3(
        idx % is.x,
        (idx / is.x) % is.y,
        idx / (is.x * is.y)
    );

    //if (imageLoad(uTexDistanceField, ip).r != dis)
    //    return; // wrong distance

    uint ptIdx = imageLoad(uTexNearestPoint, ip).r;
    vec3 refp = points[ptIdx].xyz;

    // crust
    if (distance(refp, ip) > uCrustSize)
        return;

    checkNeighbor(ip, 0, 1, 0, refp, ptIdx);
    checkNeighbor(ip, 0, -1, 0, refp, ptIdx);
    checkNeighbor(ip, 0, 0, 1, refp, ptIdx);
    checkNeighbor(ip, 0, 0, -1, refp, ptIdx);
    checkNeighbor(ip, 1, 0, 0, refp, ptIdx);
    checkNeighbor(ip, -1, 0, 0, refp, ptIdx);
}
