#pragma once

#include <vector>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class DistanceFieldSample : public glow::glfw::GlfwApp
{
private:
    const int Size = 256;

    // data
    std::vector<glm::vec4> mPoints;
    glow::SharedVertexArray mPointVAO;
    glow::SharedArrayBuffer mPointAB;
    glow::SharedShaderStorageBuffer mPointBuffer;
    glow::SharedTexture3D mTexDistanceField;
    glow::SharedTexture3D mTexNearestPoint;

    // rendering
    glow::SharedProgram mSliceShader;
    glow::SharedVertexArray mQuadVAO;

    // marching
    glow::SharedProgram mClearShader;
    glow::SharedProgram mInitShader;
    glow::SharedProgram mMarchShader;
    glow::SharedPrimitiveQuery mFeedbackQuery;
    glow::SharedPrimitiveQuery mGeneratedQuery;
    glow::SharedOcclusionQuery mFragmentsQuery;
    glow::SharedVertexArray mVAOQueueA;
    glow::SharedVertexArray mVAOQueueB;
    glow::SharedArrayBuffer mQueueA;
    glow::SharedArrayBuffer mQueueB;
    glow::SharedTransformFeedback mFeedbackA;
    glow::SharedTransformFeedback mFeedbackB;
    glow::SharedTimestamp mGpuTimerStart;
    glow::SharedTimestamp mGpuTimerEnd;
    glow::SharedTimerQuery mGpuTimer;

    float mCutY = 0.492f;
    float mYAnim = 0.0;
    bool mShowSlice = true;

    bool mRecalcEveryFrame = false;

    float mCrustSize = 1000.0f;

public:
    void recompute();

protected:
    void init() override;
    void update(float elapsedSeconds) override;

    void onRenderOpaquePass(glow::pipeline::RenderContext const& ctx) override;

public:
    DistanceFieldSample() : GlfwApp(Gui::AntTweakBar) {}
};
