uniform sampler2DRect uTexture;
uniform sampler2DRect uTexNormal;
uniform sampler2DRect uTexDepth;
uniform sampler2DRect uTexPosition;

uniform float uOutlineNormal;
uniform float uOutlineDepth;

uniform vec3 uOutlineColor;

uniform mat4 uView;

float zOf(vec3 worldPos) 
{
    return -vec4(uView * vec4(worldPos, 1.0)).z;
}

out vec4 fColor;

bool isEdge(ivec2 uv) 
{
    vec3 n = texelFetch(uTexNormal, uv).xyz;
    vec3 n1 = texelFetch(uTexNormal, uv + ivec2(1,0)).xyz;
    vec3 n2 = texelFetch(uTexNormal, uv + ivec2(-1,0)).xyz;
    vec3 n3 = texelFetch(uTexNormal, uv + ivec2(0,1)).xyz;
    vec3 n4 = texelFetch(uTexNormal, uv + ivec2(0,-1)).xyz;

    float d = zOf(texelFetch(uTexPosition, uv).xyz);
    float d1 = zOf(texelFetch(uTexPosition, uv + ivec2(1,0)).xyz);
    float d2 = zOf(texelFetch(uTexPosition, uv + ivec2(-1,0)).xyz);
    float d3 = zOf(texelFetch(uTexPosition, uv + ivec2(0,1)).xyz);
    float d4 = zOf(texelFetch(uTexPosition, uv + ivec2(0,-1)).xyz);

    float e = 0.0;

    e += distance(n, n1) * uOutlineNormal;
    e += distance(n, n2) * uOutlineNormal;
    e += distance(n, n3) * uOutlineNormal;
    e += distance(n, n4) * uOutlineNormal;

    e += abs(d - d1) * uOutlineDepth;
    e += abs(d - d2) * uOutlineDepth;
    e += abs(d - d3) * uOutlineDepth;
    e += abs(d - d4) * uOutlineDepth;

    return e > 1;
}

void main() 
{
    vec4 color = texelFetch(uTexture, ivec2(gl_FragCoord.xy));

    color = mix(color, vec4(uOutlineColor,1), float(isEdge(ivec2(gl_FragCoord.xy))));

    fColor = color;
}
