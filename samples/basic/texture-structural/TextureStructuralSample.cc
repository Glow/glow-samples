#include "TextureStructuralSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/ColorSpace.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void TextureStructuralSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    auto cube = geometry::Cube<>().generate();
    mCube = VertexArray::create(cube->getAttributeBuffer("aPosition"), GL_PATCHES);
    mCube->setVerticesPerPatch(4);

    mTexColor = Texture2D::createFromFile(util::pathOf(__FILE__) + "/cobble.color.png", ColorSpace::sRGB);
    mTexNormal = Texture2D::createFromFile(util::pathOf(__FILE__) + "/cobble.normal.png", ColorSpace::Linear);
    mTexHeight = Texture2D::createFromFile(util::pathOf(__FILE__) + "/cobble.height.png", ColorSpace::Linear);

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});
    cam->setNearPlane(0.001f);

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");
    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "");
    TwAddVarRW(tweakbar(), "Light Dis", TW_TYPE_FLOAT, &mLightDis, "step=0.01 min=1.0 max=100.0");
    TwAddVarRW(tweakbar(), "Apparent Depth", TW_TYPE_FLOAT, &mApparentDepth, "step=0.01 min=-1.0 max=1.0");
    TwAddVarRW(tweakbar(), "Displacement", TW_TYPE_FLOAT, &mDisplacement, "step=0.01 min=-1.0 max=1.0");
    TwAddVarRW(tweakbar(), "% Ambient", TW_TYPE_FLOAT, &mAmbient, "step=0.01 min=0.0 max=1.0");
    TwAddVarRW(tweakbar(), "% Diffuse", TW_TYPE_FLOAT, &mDiffuse, "step=0.01 min=0.0 max=1.0");
    TwAddVarRW(tweakbar(), "% Specular", TW_TYPE_FLOAT, &mSpecular, "step=0.01 min=0.0 max=1.0");
    TwAddVarRW(tweakbar(), "Shininess", TW_TYPE_FLOAT, &mShininess, "step=0.1 min=0.1 max=128.0");
    TwAddVarRW(tweakbar(), "Normal LOD Bias", TW_TYPE_FLOAT, &mNormalLODBias, "step=0.1 min=0.0 max=10.0");
    TwAddVarRW(tweakbar(), "Parallax Samples", TW_TYPE_INT32, &mParallaxSamples, "min=1 max=128");
    TwAddVarRW(tweakbar(), "Tessellation Level", TW_TYPE_FLOAT, &mTessellationLevel, "step=0.1 min=1.0 max=64.0");
    TwAddVarRW(tweakbar(), "Show Normals", TW_TYPE_BOOLCPP, &mShowNormals, "");
    TwAddVarRW(tweakbar(), "Show Wireframe", TW_TYPE_BOOLCPP, &mShowWireframe, "");
    TwAddVarRW(tweakbar(), "Use Microfacets", TW_TYPE_BOOLCPP, &mUseMicrofacetModel, "");
    TwAddVarRW(tweakbar(), "Derive Normals", TW_TYPE_BOOLCPP, &mDeriveNormals, "");
    TwAddVarRW(tweakbar(), "Derive Tangents", TW_TYPE_BOOLCPP, &mDeriveTangents, "");

    TwEnumVal techniqueEV[] = {
        {(int)Technique::None, "Default Phong"},        //
        {(int)Technique::ColorOnly, "Color Only"},      //
        {(int)Technique::NormalOnly, "Normal Only"},    //
        {(int)Technique::HeightOnly, "Height Only"},    //
        {(int)Technique::Bump, "Bump Mapping"},         //
        {(int)Technique::Normal, "Normal Mapping"},     //
        {(int)Technique::Parallax, "Parallax Mapping"}, //
    };
    auto techniqueType = TwDefineEnum("Technique", techniqueEV, 7);
    TwAddVarRW(tweakbar(), "Technique", techniqueType, &mTechnique, "");

    TwDefine("Tweakbar size='300 400' valueswidth=140");
}

void TextureStructuralSample::render(float elapsedSeconds)
{
    if (mAnimate)
        mRuntime += elapsedSeconds;


    {
        auto t = mTexNormal->bind();
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, mNormalLODBias);
    }

    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0.00f, 0.33f, 0.66f, 1);
    // GLOW_SCOPED(clearColor, 1,1,1, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_CULL_FACE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", mRuntime);
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uModel", rotate(mRuntime, glm::vec3{0, 1, 0}));
    shader.setTexture("uTexColor", mTexColor);
    shader.setTexture("uTexNormal", mTexNormal);
    shader.setTexture("uTexHeight", mTexHeight);
    shader.setUniform("uTechnique", (int)mTechnique);
    shader.setUniform("uApparentDepth", mApparentDepth);
    shader.setUniform("uDisplacement", mDisplacement);
    shader.setUniform("uLightPos", mLightDis * normalize(mLightDir));
    shader.setUniform("uCamPos", cam->getPosition());
    shader.setUniform("uCamUp", cam->handle.getTransform().getUpVector());
    shader.setUniform("uCamRight", cam->handle.getTransform().getRightVector());
    shader.setUniform("uAmbient", mAmbient);
    shader.setUniform("uDiffuse", mDiffuse);
    shader.setUniform("uSpecular", mSpecular);
    shader.setUniform("uShininess", mShininess);
    shader.setUniform("uShowNormals", mShowNormals);
    shader.setUniform("uParallaxSamples", mParallaxSamples);
    shader.setUniform("uTessellationLevel", mTessellationLevel);
    shader.setUniform("uUseMicrofacetModel", mUseMicrofacetModel);
    shader.setUniform("uDeriveNormals", mDeriveNormals);
    shader.setUniform("uDeriveTangents", mDeriveTangents);

    GLOW_SCOPED(polygonMode, mShowWireframe ? GL_LINE : GL_FILL);

    mCube->bind().draw();
}
