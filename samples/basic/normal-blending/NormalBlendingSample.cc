#include "NormalBlendingSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void NormalBlendingSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mCube = geometry::Cube<>().generate();

    mNormalsBase = Texture2D::createFromFile(util::pathOf(__FILE__) + "/base_160.png", ColorSpace::Linear);
    mNormalsDetail = Texture2D::createFromFile(util::pathOf(__FILE__) + "/detail_160.png", ColorSpace::Linear);

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});

    TwAddVarRW(tweakbar(), "Show Normals", TW_TYPE_BOOLCPP, &mShowNormals, "");
    TwAddVarRW(tweakbar(), "Switch Maps", TW_TYPE_BOOLCPP, &mSwitchMaps, "");
    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "");

    TwEnumVal modeEV[] = {
        {(int)BlendMode::MacroOnly, "Macro Only"},             //
        {(int)BlendMode::DetailOnly, "Detail Only"},           //
        {(int)BlendMode::Linear, "Linear"},                    //
        {(int)BlendMode::PartialDeriv, "Partial Derivatives"}, //
        {(int)BlendMode::Whiteout, "Whiteout"},                //
        {(int)BlendMode::RNM, "Reoriented Normal Mapping"},    //
    };

    TwAddVarRW(tweakbar(), "Blend Mode", TwDefineEnum("Blend Mode", modeEV, 6), &mMode, "");
}

void NormalBlendingSample::render(float elapsedSeconds)
{
    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());

    shader.setUniform("uLightDir", normalize(mLightDir));
    shader.setUniform("uCamPos", cam->getPosition());

    shader.setUniform("uShowNormals", mShowNormals);
    shader.setUniform("uSwitchMaps", mSwitchMaps);

    shader.setTexture("uNormalsBase", mNormalsBase);
    shader.setTexture("uNormalsDetail", mNormalsDetail);

    for (int i = 0; i < 3; ++i)
    {
        shader.setUniform("uModel", translate(glm::vec3((i - 1) * 3, 0, 0)));

        shader.setUniform("uPos", i);
        shader.setUniform("uMode", (int)mMode);

        mCube->bind().draw();
    }
}
