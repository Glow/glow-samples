#include "FullscreenQuadSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/Shader.hh>
#include <glow-extras/geometry/Quad.hh>

#include <AntTweakBar.h>

using namespace glow;

void FullscreenQuadSample::init()
{
    GlfwApp::init(); // call to base!

    mQuad = geometry::Quad<>().generate();

    auto vertexShader
        = Shader::createFromSource(GL_VERTEX_SHADER, "uniform float uRuntime = .5;\n"
                                                     "in vec2 aPosition;\n"
                                                     "out vec2 vPosition;\n"
                                                     "void main() {\n"
                                                     "  vPosition = aPosition * .5 + .5;\n"
                                                     "  gl_Position = vec4(aPosition * cos(uRuntime), 0.0, 1.0);\n"
                                                     "}\n");

    auto fragmentShader = Shader::createFromSource(GL_FRAGMENT_SHADER, "in vec2 vPosition;\n"
                                                                       "out vec3 fColor;\n"
                                                                       "void main() {\n"
                                                                       "  fColor = vec3(vPosition,0);\n"
                                                                       "}\n");

    mShader = Program::create({vertexShader, fragmentShader});
}

void FullscreenQuadSample::render(float elapsedSeconds)
{
    // Accumulate elapsed time
    mRuntime += elapsedSeconds;

    // Clear in RWTH blue
    glClearColor(0 / 255.f, 84 / 255.f, 159 / 255.f, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    // Set shader uniform and draw the quad
    auto shader = mShader->use();
    shader.setUniform("uRuntime", mRuntime);
    auto quad = mQuad->bind();
    quad.draw();
}
