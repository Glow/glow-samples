in vec3 aPosition;
in vec3 aNormal;

out vec3 vNormal;
out vec3 vWorldPos;

void main() 
{
    vNormal = aNormal;
    vWorldPos = aPosition;
    gl_Position = vec4(aPosition, 1.0);
}
