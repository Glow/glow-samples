uniform vec3 uLightDir;
uniform bool uShowNormals;

in vec3 tWorldPos;
in vec3 tNormal;

out vec3 fColor;

void main()
{
    fColor = vec3(1, 1, 1) * (dot(normalize(tNormal), normalize(uLightDir)) * 0.5 + 0.5);
    if (uShowNormals)
        fColor = tNormal;
}