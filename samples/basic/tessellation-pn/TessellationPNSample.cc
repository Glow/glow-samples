#include "TessellationPNSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/ColorSpace.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void TessellationPNSample::addMesh(const std::string &name)
{
    auto path = util::pathOf(__FILE__) + "/" + name + ".obj";
    auto mesh = assimp::Importer().load(path);
    mesh->setPrimitiveMode(GL_PATCHES);
    mesh->setVerticesPerPatch(3);
    mMeshes.push_back(mesh);
}

void TessellationPNSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");

    addMesh("cube");
    addMesh("monkey");

    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "");
    TwAddVarRW(tweakbar(), "Show Wireframe", TW_TYPE_BOOLCPP, &mWireframe, "");
    TwAddVarRW(tweakbar(), "Show Normals", TW_TYPE_BOOLCPP, &mShowNormals, "");
    TwAddVarRW(tweakbar(), "Use PN", TW_TYPE_BOOLCPP, &mUsePN, "");
    TwAddVarRW(tweakbar(), "Mesh", TW_TYPE_INT32, &mMeshIdx, "min=0 max=1");
    TwAddVarRW(tweakbar(), "Tessellation Level", TW_TYPE_FLOAT, &mTessellationLevel, "step=0.1 min=1.0 max=64.0");
}

void TessellationPNSample::render(float elapsedSeconds)
{
    GLOW_SCOPED(clearColor, glm::vec3(0.00, 0.33, 0.66));
    // GLOW_SCOPED(clearColor, 1,1,1, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_CULL_FACE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto cam = getCamera();

    auto shader = mShader->use();
    shader.setUniform("uTessellationLevel", mTessellationLevel);
    shader.setUniform("uUsePN", mUsePN);
    shader.setUniform("uShowNormals", mShowNormals);
    shader.setUniform("uLightDir", normalize(mLightDir));
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uView", cam->getViewMatrix());

    GLOW_SCOPED(wireframe, mWireframe);

    mMeshes[mMeshIdx]->bind().draw();
}
