#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class TessellationPNSample : public glow::glfw::GlfwApp
{
public:
    TessellationPNSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;

    std::vector<glow::SharedVertexArray> mMeshes;

    glm::vec3 mLightDir = normalize(glm::vec3(-.2, 1, .2));
    bool mWireframe = false;
    bool mShowNormals = false;
    bool mUsePN = true;
    int mMeshIdx = 0;
    float mTessellationLevel = 16.0f;

protected:
    void addMesh(std::string const& name);
    void init() override;
    void render(float elapsedSeconds) override;
};
