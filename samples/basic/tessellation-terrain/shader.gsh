layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform float uWidth;
uniform float uHeight;
uniform mat4 uView;

in vec3 tWorldPos[];
in float tWorldEdge[];

out float gEdgeLength;
out float gWorldEdge;
out vec3 gWorldPos;
out vec3 gViewPos;
out vec4 gScreenPos;

void main()
{
    vec2 sc0 = gl_in[0].gl_Position.xy / gl_in[0].gl_Position.w;
    vec2 sc1 = gl_in[1].gl_Position.xy / gl_in[1].gl_Position.w;
    vec2 sc2 = gl_in[2].gl_Position.xy / gl_in[2].gl_Position.w;

    sc0 = (sc0 * 0.5 + 0.5) * vec2(uWidth, uHeight);
    sc1 = (sc1 * 0.5 + 0.5) * vec2(uWidth, uHeight);
    sc2 = (sc2 * 0.5 + 0.5) * vec2(uWidth, uHeight);
    
    gEdgeLength = (distance(sc0, sc1) + distance(sc1, sc2) + distance(sc2, sc0)) / 3.0;

    for (int i = 0; i < 3; ++i)
    {
        gl_Position = gScreenPos = gl_in[i].gl_Position;
        gWorldEdge = tWorldEdge[i];
        gWorldPos = tWorldPos[i];
        gViewPos = vec3(uView * vec4(tWorldPos[i], 1.0));
        EmitVertex();
    }

    EndPrimitive();
}
