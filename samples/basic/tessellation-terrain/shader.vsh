#include "terrain.glsl"

uniform int uQuads;
uniform float uQuadSize;

in vec3 aPosition;

void main() 
{
    int id = gl_InstanceID;
    int x = id % uQuads;
    int y = id / uQuads;

    vec3 pos = aPosition;
    pos += vec3(x, 0, y);
    pos -= vec3(uQuads, 0, uQuads) / 2.0;
    pos *= uQuadSize;

    pos.y = terrainAt(pos.x, pos.z, uQuadSize / 64.0);

    gl_Position = vec4(pos, 1.0);
}
