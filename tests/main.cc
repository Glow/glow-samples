#include <nexus/Nexus.hh>

#include <glow-extras/glfw/GlfwContext.hh>

int main(int argc, char** argv)
{
    glow::glfw::GlfwContext ctx;

    nx::Nexus nx;
    nx.applyCmdArgs(argc, argv);
    return nx.run();
}
