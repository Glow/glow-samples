#include <nexus/test.hh>

#include <glow/common/gl_helper.hh>
#include <glow/common/scoped_gl.hh>

TEST("scoped gl")
{
    using namespace glow;

    restoreDefaultOpenGLState();

    // enable-disable
    CHECK(!glIsEnabled(GL_BLEND));
    CHECK(!glIsEnabled(GL_DEPTH_TEST));

    {
        scoped::enable _blend(GL_BLEND);
        scoped::enable _depth(GL_DEPTH_TEST);

        CHECK(glIsEnabled(GL_BLEND));
        CHECK(glIsEnabled(GL_DEPTH_TEST));

        {
            scoped::disable _blend(GL_BLEND);

            CHECK(!glIsEnabled(GL_BLEND));
            CHECK(glIsEnabled(GL_DEPTH_TEST));
        }

        CHECK(glIsEnabled(GL_BLEND));
        CHECK(glIsEnabled(GL_DEPTH_TEST));
    }

    CHECK(!glIsEnabled(GL_BLEND));
    CHECK(!glIsEnabled(GL_DEPTH_TEST));

    // functions
    CHECK(glGetEnum(GL_CULL_FACE_MODE) == (GLenum)GL_BACK);
    {
        scoped::cullFace _(GL_FRONT);
        CHECK(glGetEnum(GL_CULL_FACE_MODE) == (GLenum)GL_FRONT);
        {
            scoped::cullFace _(GL_FRONT_AND_BACK);
            CHECK(glGetEnum(GL_CULL_FACE_MODE) == (GLenum)GL_FRONT_AND_BACK);
        }
        CHECK(glGetEnum(GL_CULL_FACE_MODE) == (GLenum)GL_FRONT);
    }
    CHECK(glGetEnum(GL_CULL_FACE_MODE) == (GLenum)GL_BACK);

    // pixel store
    CHECK(glGetInt(GL_PACK_ALIGNMENT) == 4);
    {
        scoped::packAlignment _(1);
        CHECK(glGetInt(GL_PACK_ALIGNMENT) == 1);
        {
            scoped::packAlignment _(2);
            CHECK(glGetInt(GL_PACK_ALIGNMENT) == 2);
        }
        CHECK(glGetInt(GL_PACK_ALIGNMENT) == 1);
    }
    CHECK(glGetInt(GL_PACK_ALIGNMENT) == 4);
}
