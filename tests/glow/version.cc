#include <nexus/test.hh>

#include <glow/gl.hh>
#include <glow/glow.hh>

TEST("Glow, Version")
{
    CHECK(GLVersion.major >= 4);
    CHECK(GLVersion.minor >= 3);
}
