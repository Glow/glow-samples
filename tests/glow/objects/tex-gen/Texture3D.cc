// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture3D.hh>

using namespace glow;

TEST("Texture3D, Binding")
{
    auto tex0 = Texture3D::create();
    CHECK(Texture3D::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(Texture3D::getCurrentTexture() == &btex0);

        auto tex1 = Texture3D::create();
        auto tex2 = Texture3D::create();
        CHECK(Texture3D::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(Texture3D::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(Texture3D::getCurrentTexture() == &btex2);
        }

        CHECK(Texture3D::getCurrentTexture() == &btex0);
    }

    CHECK(Texture3D::getCurrentTexture() == nullptr);
}
