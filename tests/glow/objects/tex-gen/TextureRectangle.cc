// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/TextureRectangle.hh>

using namespace glow;

TEST("TextureRectangle, Binding")
{
    auto tex0 = TextureRectangle::create();
    CHECK(TextureRectangle::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(TextureRectangle::getCurrentTexture() == &btex0);

        auto tex1 = TextureRectangle::create();
        auto tex2 = TextureRectangle::create();
        CHECK(TextureRectangle::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(TextureRectangle::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(TextureRectangle::getCurrentTexture() == &btex2);
        }

        CHECK(TextureRectangle::getCurrentTexture() == &btex0);
    }

    CHECK(TextureRectangle::getCurrentTexture() == nullptr);
}
