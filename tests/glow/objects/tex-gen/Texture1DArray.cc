// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture1DArray.hh>

using namespace glow;

TEST("Texture1DArray, Binding")
{
    auto tex0 = Texture1DArray::create();
    CHECK(Texture1DArray::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(Texture1DArray::getCurrentTexture() == &btex0);

        auto tex1 = Texture1DArray::create();
        auto tex2 = Texture1DArray::create();
        CHECK(Texture1DArray::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(Texture1DArray::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(Texture1DArray::getCurrentTexture() == &btex2);
        }

        CHECK(Texture1DArray::getCurrentTexture() == &btex0);
    }

    CHECK(Texture1DArray::getCurrentTexture() == nullptr);
}
