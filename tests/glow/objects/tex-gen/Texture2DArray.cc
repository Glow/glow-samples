// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture2DArray.hh>

using namespace glow;

TEST("Texture2DArray, Binding")
{
    auto tex0 = Texture2DArray::create();
    CHECK(Texture2DArray::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(Texture2DArray::getCurrentTexture() == &btex0);

        auto tex1 = Texture2DArray::create();
        auto tex2 = Texture2DArray::create();
        CHECK(Texture2DArray::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(Texture2DArray::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(Texture2DArray::getCurrentTexture() == &btex2);
        }

        CHECK(Texture2DArray::getCurrentTexture() == &btex0);
    }

    CHECK(Texture2DArray::getCurrentTexture() == nullptr);
}
