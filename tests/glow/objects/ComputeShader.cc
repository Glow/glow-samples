#include <nexus/test.hh>

#include <glm/vec4.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>

using namespace glow;

static std::string csSource = R"src(#version 430 core

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

uniform layout(rgba32f, binding=0) writeonly image2D uTex;
uniform float uSingle;
uniform float uArray[4];

void main() {
    uint x = gl_WorkGroupID.x;
    uint y = gl_WorkGroupID.y;
    imageStore(uTex, ivec2(x, y), vec4(x, y, uSingle, uArray[y % 4]));
}

)src";


TEST("ComputeShader, Simple")
{
    auto cs = Shader::createFromSource(GL_COMPUTE_SHADER, csSource);
    CHECK(cs->isCompiledWithoutErrors());

    auto prog = Program::create(cs);

    auto tex = Texture2D::createStorageImmutable(4, 4, GL_RGBA32F);

    {
        auto uprog = prog->use();

        uprog.setImage(0, tex, GL_WRITE_ONLY);

        uprog.setUniform("uSingle", 17.f);
        uprog["uArray"] = {5.f, 6.f, 7.f, 8.f};

        uprog.compute(tex->getWidth(), tex->getHeight());
    }

    auto data = tex->bind().getData<glm::vec4>();
    for (auto y = 0; y < tex->getHeight(); ++y)
        for (auto x = 0; x < tex->getWidth(); ++x)
        {
            auto val = data[y * tex->getWidth() + x];
            CHECK(val.x == x);
            CHECK(val.y == y);
            CHECK(val.z == 17.f);
            CHECK(val.w == 5.f + y);
        }
}
